libs = 
src = 	sources/main.cpp \
        sources/test_thread.cpp \
        sources/test_functional.cpp \
        sources/problem_set_1.cpp \
        sources/tim.cpp \
        sources/test_io.cpp \
        sources/sutter.cpp \
        sources/linux_io.cpp \
        sources/test_memory.cpp \
        sources/test_atomic.cpp \
        sources/test_collections.cpp \

name = cpp11
out = ./bin

CPPFLAGS += -std=c++0x -O0 -DDEBUG -g

all: $(src) | dirs
	$(CXX) $(src) $(CPPFLAGS) -o $(out)/$(name) $(libs)

dirs: 
	-mkdir -p $(out)
