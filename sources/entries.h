#pragma once

extern void run_test_thread();
extern void run_problem_set_1();
extern void run_test_functional();
extern void run_tim();
extern void run_test_io();
extern void run_sutter();
extern void run_linux_io();
extern void run_test_memory();
extern void run_test_atomic();
extern void run_test_collections();
