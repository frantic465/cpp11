#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <fstream>

void test_fds() {
    const char* filename = "test.txt";
    const int flags = O_WRONLY | O_CREAT;
    const int mode = S_IRWXU | S_IRWXG;

    int fd = open(filename, flags, mode);
    if (fd == -1) {
        perror("open");
        return;
    }

    if (close(fd) == -1) {
        perror("close");
        return;
    }
}

int read_data(int fd, char* buffer, int size) {
    int len = size;
    while (len != 0) {
        int ret = read(fd, buffer, len);
        if (ret == 0) {
            return size - len;
        }
        if (ret == -1) {
            if (errno == EINTR) {
                continue;
            }
            perror("read");
            return -1;
        }
        len -= ret;
        buffer += ret;
    }
    return size - len;
}

void test_pipes0() {

    int pfd[2];
    if (pipe(pfd) == -1) {
        perror("pipe");
        return;
    }

    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        return;
    } else if (pid == 0) {
        close(pfd[0]);
        std::string message = "Hello Dad!";
        int ret = write(pfd[1], message.data(), message.size());
        if (ret == -1) {
            perror("write");
            return;
        }
        close(pfd[1]);
        exit(0);
    } else {
        close(pfd[1]);
        std::string message;
        message.resize(512);
        int ret = read_data(pfd[0], &message[0], message.size());
        message.resize(ret);
        std::cout << "Received message: " << message << std::endl;
        close(pfd[0]);
        waitpid(pid, NULL, 0);
    }
}

void test_pipes1() {

    const char* cmdline = "echo 123";
    FILE* fp = popen(cmdline, "r");

    std::string buffer;
    buffer.resize(128);

    int ret = fread(&buffer[0], 1, buffer.size(), fp);
    buffer.resize(ret);

    std::cout << "Received message: " << buffer << std::endl;

    pclose(fp);
}

void run_linux_io() {
    std::cout << "linux_io >>>> " << std::endl;

    //test_fds();

    test_pipes0();
    test_pipes1();

    std::cout << "linux_io <<<< " << std::endl;
}
