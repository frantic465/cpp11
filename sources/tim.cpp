#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <cstring>
#include <algorithm>
#include <limits>
#include <stack>
#include <queue>
#include <future>
#include <time.h>

namespace tim {

class IllegalStateException : public std::exception {
public:
    explicit IllegalStateException(std::string message) : message(message) {}
    virtual const char* what() const _NOEXCEPT {
        return message.c_str();
    }
private:
    std::string message;
};

template <typename T>
void merge_sort(std::vector<T>& a) {
    class MergeSortImpl {
    public:
        static void sort(std::vector<T>& a, int begin, int end) {
            const int n = end - begin;
            if (n <= 1) {
                return;
            }

            sort(a, begin, begin + n / 2);
            sort(a, begin + n / 2, end);
            merge(a, begin, begin + n / 2, end);
        }

        static void merge(std::vector<T>& a, int begin, int middle, int end) {
            std::vector<T> result;
            result.reserve(end - begin);
            int leftIt = begin;
            int rightIt = middle;
            while(leftIt != middle || rightIt != end) {
                T value;
                if (leftIt == middle) {
                    value = a[rightIt++];
                } else if (rightIt == end) {
                    value = a[leftIt++];
                } else if (a[leftIt] < a[rightIt]) {
                    value = a[leftIt++];
                } else {
                    value = a[rightIt++];
                }
                result.push_back(value);
            }

            std::copy(result.begin(), result.end(), a.begin() + begin);
        }
    };

    MergeSortImpl::sort(a, 0, a.size());
}

template <typename T>
void insertion_sort(std::vector<T>& a) {
    const int size = a.size();
    for (int i = 1; i < size; ++i) {
        const int x = a[i];
        int j = i - 1;
        while (j >= 0 && a[j] > x) {
            std::swap(a[j], a[j + 1]);
            --j;
        }
    }
}

template <typename T>
void quick_sort(std::vector<T>& a) {
    class QuickSortImpl {
    public:
        static void sort(std::vector<T>& a, int begin, int end) {
            const int n = end - begin;
            if (n <= 1) {
                return;
            }

            int pivot = getPivot(a, begin, end);
            std::swap(a[begin], a[pivot]);

            int i = 1;
            int k = 1;
            for (int i = begin + 1; i < end; ++i) {
                if (a[i] < a[begin]) {
                    std::swap(a[i], a[begin + k]);
                    ++k;
                }
            }
            pivot = begin + k - 1;
            std::swap(a[begin], a[pivot]);

            sort(a, begin, pivot);
            sort(a, pivot + 1, end);
        }

        static int getPivot(std::vector<T>& a, int begin, int end) {
            return rand() % (end - begin) + begin;
        }
    };

    QuickSortImpl::sort(a, 0, a.size());
}

template <typename T>
T get_nth_statictic(std::vector<T>& a, int index) {
    class QuickSortBaseImpl {
    public:
        static T get_nth_statistic(std::vector<T>& a, int begin, int end, int index) {
            const int n = end - begin;
            if (n == 0) {
                throw std::exception();
            }
            if (n == 1) {
                if (begin == index) {
                    return a[begin];
                } else {
                    throw std::exception();
                }
            }

            int pivot = getPivot(a, begin, end);
            std::swap(a[begin], a[pivot]);

            int i = 1;
            int k = 1;
            for (int i = begin + 1; i < end; ++i) {
                if (a[i] < a[begin]) {
                    std::swap(a[i], a[begin + k]);
                    ++k;
                }
            }
            pivot = begin + k - 1;
            std::swap(a[begin], a[pivot]);

            if (index == pivot) {
                return a[pivot];
            }
            if (index < pivot) {
                return get_nth_statistic(a, begin, pivot, index);
            }
            return get_nth_statistic(a, pivot + 1, end, index);
        }

        static int getPivot(std::vector<T>& a, int begin, int end) {
            return rand() % (end - begin) + begin;
        }
    };
    std::vector<T> copy(a);
    return QuickSortBaseImpl::get_nth_statistic(copy, 0, copy.size(), index);
}

void graph_minimum_cut() {

}

template <int N, bool Directed = false>
class FixedSizeGraph {

public:

    FixedSizeGraph() {
        std::memset(edges, 0, sizeof(edges));
    }

    void print() {
        std::cout << "edges: { ";
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                if (edges[i][j]) {
                    std::cout << "(" << i << ", " << j << "), ";
                }
            }
        }
        std::cout << "}" << std::endl;
    }

    void add(int i, int j, int weight = 1) {
        if (i >= N || j >= N) {
            throw std::exception();
        }
        edges[i][j] = weight;
        if (!Directed) {
            edges[j][i] = weight;
        }
    }

private:
    int edges[N][N];
};


template <bool Directed = false>
class Graph {
private:

    struct Vertex {
        std::list<int> edges;
        int id;
    };

    struct Edge {
        int id;
        int from, to;
        int weight;
    };

public:

    explicit Graph(int numberOfVertices) : vertices(), edges() {
        for (int i = 0; i < numberOfVertices; ++i) {
            vertices.push_back(Vertex{.id = i});
        }
    }

    explicit Graph(const Graph& g) : vertices(g.vertices), edges(g.edges) {
    }

    void addEdge(int i, int j, int weight = 0) {


        if (i < 0 || i >= vertices.size()) {
            throw IllegalStateException("Invalid vertex id");
        }

        if (j < 0 || j >= vertices.size()) {
            throw IllegalStateException("Invalid vertex id");
        }

        Edge edge = {.id = (int)edges.size(), .from = i, .to = j, .weight = weight};
        edges.push_back(edge);

        vertices[i].edges.push_back(edge.id);
        vertices[j].edges.push_back(edge.id);
    }

    void print() {
        std::cout << "vertices: {" ;
        for (const Vertex& v : vertices) {
            std::cout << v.id << ", ";
        }
        std::cout << "}" << std::endl;
        std::cout << "edges: {";
        for (const Edge& edge : edges) {
            std::cout << "(" << edge.from << ", " << edge.to << "), ";
        }
        std::cout << "}" << std::endl;
    }

public:
    int find_minimum_cut() {

        class RandomizedContraction {
        public:
            static int find_minimum_cut(const Graph& graph) {
                std::map<int, Vertex> verticesTmp;
                for (const Vertex& v : graph.vertices) {
                    verticesTmp[v.id] = v;
                }

                std::map<int, Edge> edgesTmp;
                std::vector<int> edgesIds;
                for (const Edge& e : graph.edges) {
                    edgesTmp[e.id] = e;
                    edgesIds.push_back(e.id);
                }

                while (verticesTmp.size() > 2) {
                    const int edgesNumber = edgesIds.size();
                    const int random = rand() % edgesNumber;

                    Edge edgeToRemove = edgesTmp[edgesIds[random]];
                    edgesTmp.erase(edgeToRemove.id);
                    edgesIds.erase(std::remove(edgesIds.begin(), edgesIds.end(), edgeToRemove.id), edgesIds.end());

                    Vertex from = verticesTmp[edgeToRemove.from];
                    Vertex& to = verticesTmp[edgeToRemove.to];
                    verticesTmp.erase(from.id);

                    to.edges.remove(edgeToRemove.id);

                    for (auto it = from.edges.begin(); it != from.edges.end(); ++it) {
                        if (*it == edgeToRemove.id) {
                            continue;
                        }
                        Edge& edge = edgesTmp[*it];
                        if (edge.from == from.id) {
                            edge.from = to.id;
                        } else if (edge.to == from.id) {
                            edge.to = to.id;
                            to.edges.push_back(edge.id);
                        }

                        if (std::find(to.edges.begin(), to.edges.end(), edge.id) == to.edges.end()) {
                            to.edges.push_back(edge.id);
                        }
                    }

                    for (auto it = to.edges.begin(); it != to.edges.end(); ) {
                        auto curIt = it++;
                        Edge edge = edgesTmp[*curIt];
                        if (edge.from == edge.to) {
                            it = to.edges.erase(curIt);
                            edgesTmp.erase(edge.id);
                            edgesIds.erase(std::remove(edgesIds.begin(), edgesIds.end(), edge.id), edgesIds.end());
                        }
                    }
                }

                return edgesTmp.size();
            }
        };

        int result = std::numeric_limits<int>::max();
        for (int i = 0; i < vertices.size()*vertices.size(); ++i) {
            int tmp = RandomizedContraction::find_minimum_cut(*this);
            if (tmp < result) {
                result = tmp;
            }
        }
        return result;
    }

    void bfs(int startVertex, std::function<void(int)> f) {
        class NonRecursiveImpl {
        private:
            const int startVertex;
            const Graph& graph;
            const std::function<void(int)>& f;
        public:
            explicit NonRecursiveImpl(const Graph& graph, int startVertex, std::function<void(int)> f)
                : graph(graph),
                  startVertex(startVertex),
                  f(f)
            {}

            void bfs() {

                std::map<int, bool> explored;
                for (const Vertex& v : graph.vertices) {
                    explored[v.id] = false;
                }

                std::queue<int> q;
                q.push(startVertex);
                explored[startVertex] = true;

                while (!q.empty()) {
                    int vid = q.front();
                    q.pop();
                    f(vid);
                    const Vertex& vertex = graph.vertices.at(vid);
                    for (int edgeId : vertex.edges) {
                        const Edge& edge = graph.edges.at(edgeId);
                        int adjucent = edge.from != vid ? edge.from : edge.to;
                        if (!explored[adjucent]) {
                            explored[adjucent] = true;
                            q.push(adjucent);
                        }
                    }
                }
            }
        };

        NonRecursiveImpl(*this, startVertex, f).bfs();
    }

    void bfs_print(int startVertex) {
        std::cout << "bfs_print(n): ";
        bfs(startVertex, [](int vid){
            std::cout << vid << " ";
        });
        std::cout << std::endl;
    }

    void dfs(int startVertex, std::function<void(int)> f) {

        class NonRecursiveImpl {
        private:
            const std::function<void(int)>& f;
            const int startVertex;
            const Graph& graph;
        public:
            explicit NonRecursiveImpl(const Graph& graph, int startVertex, std::function<void(int)>& f)
                : graph(graph),
                  startVertex(startVertex),
                  f(f)
            {}

            void dfs() {

                std::vector<int> explored(graph.vertices.size());
                for (const Vertex& v : graph.vertices) {
                    explored[v.id] = false;
                }

                std::stack<int> q;
                q.push(startVertex);
                explored[startVertex] = true;

                while (!q.empty()) {
                    int vid = q.top();
                    q.pop();
                    f(vid);
                    const Vertex& vertex = graph.vertices.at(vid);
                    for (int edgeId : vertex.edges) {
                        const Edge& edge = graph.edges.at(edgeId);
                        int adjucent = edge.from != vid ? edge.from : edge.to;
                        if (!explored[adjucent]) {
                            explored[adjucent] = true;
                            q.push(adjucent);
                        }
                    }
                }
            }
        };

        class RecursiveImpl {
        private:
            std::function<void(int)>& f;
            const Graph& graph;
            int startVertex;
            std::map<int, bool> explored;
        public:
            explicit RecursiveImpl(const Graph& graph, int startVertex, std::function<void(int)>& f)
                : graph(graph),
                  startVertex(startVertex),
                  f(f)
            {
                for (const Vertex& v : graph.vertices) {
                    explored[v.id] = false;
                }
            }
            void dfs() {
                explored[startVertex] = true;
                dfs(startVertex);
            }

        private:
            void dfs(int vid) {
                f(vid);
                const Vertex& vertex = graph.vertices[vid];
                for (int edgeId : vertex.edges) {
                    const Edge& edge = graph.edges.at(edgeId);
                    int adjucent = edge.from != vid ? edge.from : edge.to;
                    if (!explored[adjucent]) {
                        explored[adjucent] = true;
                        dfs(adjucent);
                    }
                }
            }
        };

        NonRecursiveImpl(*this, startVertex, f).dfs();
        //RecursiveImpl(*this, startVertex, f).dfs();
    }

    void dfs_print(int startVertex) {

        std::cout << "dfs_print(n): ";
        dfs(startVertex, [](int i){ std::cout << i << " ";});
        std::cout << std::endl;

    }

    std::vector<int> topological_sort() {

        if (!Directed) {
            throw IllegalStateException("topological_sort can be applied only for directed graph");
        }

        class Impl {
        private:
            const Graph& graph;
            std::vector<bool> explored;
            std::vector<int> labels;
            int current_label;
        public:
            explicit Impl(const Graph& graph)
                : graph(graph),
                explored(graph.vertices.size()),
                labels(graph.vertices.size())
            {
                for (const Vertex& v : graph.vertices) {
                    explored[v.id] = false;
                    labels[v.id] = -1;
                }
                current_label = graph.vertices.size();
            }
            std::vector<int> topological_sort() {
                for (const Vertex& v : graph.vertices) {
                    if (!explored[v.id]) {
                        explored[v.id] = true;
                        topological_sort(v.id);
                    }
                }
                return labels;
            }

        private:
            void topological_sort(int vid) {
                const Vertex& vertex = graph.vertices[vid];
                for (int edgeId : vertex.edges) {
                    const Edge& edge = graph.edges.at(edgeId);

                    if (edge.from != vid) {
                        continue;
                    }

                    int adjucent = edge.to;
                    if (!explored[adjucent]) {
                        explored[adjucent] = true;
                        topological_sort(adjucent);
                    }
                }
                labels[vid] = current_label--;
            }
        };

        return Impl(*this).topological_sort();
    }

    std::vector<int> find_strongly_connected_components() {
        if (!Directed) {
            throw IllegalStateException("find_strongly_connected_components can be applied only for directed graph");
        }
        Graph invGraph(*this);
        std::for_each(invGraph.edges.begin(), invGraph.edges.end(), [](Edge& e) {
            std::swap(e.from, e.to);
        });

        std::vector<int> ts = invGraph.topological_sort();

        std::vector<int> orderedVertices(vertices.size());
        for (int i = 0; i < vertices.size(); ++i) {
            orderedVertices[i] = i;
        }
        std::sort(orderedVertices.begin(), orderedVertices.end(), [&ts](int l, int r) {
            return ts[l] < ts[r];
        });

        std::vector<int> explored(vertices.size());
        for (const Vertex& v : vertices) {
            explored[v.id] = false;
        }

        std::vector<int> labels(vertices.size(), -1);

        int currentLabel = 0;
        for (int vid : orderedVertices) {

            if (explored[vid]) {
                continue;
            }

            std::stack<int> q;
            q.push(vid);
            explored[vid] = true;

            while (!q.empty()) {
                int cur = q.top();
                q.pop();
                labels[cur] = currentLabel;
                const Vertex& vertex = vertices.at(cur);
                for (int edgeId : vertex.edges) {
                    const Edge& edge = edges.at(edgeId);
                    if (edge.from != cur) {
                        continue;
                    }
                    int adjucent = edge.to;
                    if (!explored[adjucent]) {
                        explored[adjucent] = true;
                        q.push(adjucent);
                    }
                }
            }
            ++currentLabel;
        }

        return labels;
    }

    std::vector<int> bfsShortestPath(int startVertex) {

        for (const Edge& e : edges) {
            if (e.weight != 0) {
                throw IllegalStateException("bfsShortestPath works only for non-weighted graphs");
            }
        }

        std::vector<int> distance(vertices.size(), -1);

        std::queue<int> q;
        q.push(startVertex);
        distance[startVertex] = 0;

        while (!q.empty()) {
            int vid = q.front();
            q.pop();
            const Vertex& vertex = vertices.at(vid);
            for (int edgeId : vertex.edges) {
                const Edge& edge = edges.at(edgeId);
                int adjucent = edge.from != vid ? edge.from : edge.to;
                if (distance[adjucent] == -1) {
                    distance[adjucent] = distance[vid] + 1;
                    q.push(adjucent);
                }
            }
        }

        return distance;
    }

    std::vector<int> dijkstra(int source) {

        auto genericImpl = [this](int source) {
            std::vector<int> distance(vertices.size(), -1);
            distance[source] = 0;

            for (int i = 1; i < vertices.size(); ++i) {

                int minEdge = -1;
                int minValue = -1;
                for (const Edge& e : edges) {
                    if (distance[e.from] != -1 && distance[e.to] == -1) {
                        if (minEdge == -1 || e.weight + distance[e.from] < minValue) {
                            minEdge = e.id;
                            minValue = distance[e.from] + e.weight;
                        }
                    }
                }

                if (minEdge == -1) {
                    throw IllegalStateException("Graph is not connected");
                }

                const Vertex& nextVertex = vertices[edges[minEdge].to];
                const int weight = distance[edges[minEdge].from] + edges[minEdge].weight;
                distance[nextVertex.id] = weight;
            }

            return distance;
        };

        auto heapImpl = [this](int source) {
            std::vector<int> distance(vertices.size(), -1);
            distance[source] = 0;

            struct HeapItem {
                int key, v;
            };

            typedef std::vector<HeapItem> Heap;
            Heap heap;
            auto heapComp = [](const HeapItem& x, const HeapItem& y){
                return x.key > y.key;
            };
            const int max_int = std::numeric_limits<int>::max();
            for (const Vertex& v : vertices) {
                if (v.id == source) {
                    continue;
                }
                int emin = -1;
                for (int e : v.edges) {
                    if (edges[e].from == source) {
                        if (emin == -1 || edges[e].weight < emin) {
                            emin = e;
                        }
                    }
                }
                const int key = emin != -1 ? edges[emin].weight : max_int;
                heap.push_back(HeapItem{key, v.id});
                std::push_heap(heap.begin(), heap.end(), heapComp);
            }

            for (int i = 1; i < vertices.size(); ++i) {

                std::pop_heap(heap.begin(), heap.end(), heapComp);
                const Vertex& nextVertex = vertices[heap.back().v];
                const int weight = heap.back().key;
                heap.pop_back();

                distance[nextVertex.id] = weight;

                for (int e : nextVertex.edges) {
                    if (edges[e].from == nextVertex.id) {
                        auto it = std::find_if(heap.begin(), heap.end(), [this, e](const HeapItem& heapItem) {
                            return heapItem.v == edges[e].to;
                        });
                        if (it != heap.end()) {
                            int newKey = std::min(it->key, distance[nextVertex.id] + edges[e].weight);
                            heap.erase(it);
                            heap.push_back(HeapItem{newKey, edges[e].to});
                            std::make_heap(heap.begin(), heap.end(), heapComp);
                        }
                    }
                }
            }

            return distance;
        };

        return heapImpl(source);
        //return genericImpl(source);
    }

    std::vector<std::pair<int, int>> primMST() {

        if (Directed) {
            throw IllegalStateException("Works only for undirected graphs");
        }

        auto genericImpl = [this](){
            std::vector<std::pair<int, int>> result;
            std::vector<bool> x(vertices.size(), false);
            x[0] = true;

            for (int i = 1; i < vertices.size(); ++i) {
                int minEdge = -1;
                for (const Edge& e : edges) {
                    if (x[e.from] && !x[e.to]) {
                        if (minEdge == -1 || e.weight < edges[minEdge].weight) {
                            minEdge = e.id;
                        }
                    }
                }
                x[edges[minEdge].to] = true;
                result.push_back(std::make_pair(edges[minEdge].from, edges[minEdge].to));
            }
            return result;
        };

        auto heapImpl = [this](){
            std::vector<std::pair<int, int>> result;
            std::vector<bool> x(vertices.size(), false);
            const int source = 0;
            x[source] = true;

            struct HeapItem {
                int key;
                int vid;
                int eid;
            };

            typedef std::vector<HeapItem> Heap;
            Heap heap;
            auto heapComp = [](const typename Heap::value_type& x, const typename Heap::value_type& y) {
                return x.key > y.key;
            };

            const int max_int = std::numeric_limits<int>::max();
            for (const Vertex& v : vertices) {
                if (v.id == source) {
                    continue;
                }
                int emin = -1;
                for (int e : v.edges) {
                    if (edges[e].from == source) {
                        if (emin == -1 || edges[emin].weight < emin) {
                            emin = e;
                        }
                    }
                }
                const int key = emin != -1 ? edges[emin].weight : max_int;
                heap.push_back(HeapItem{.key = key, .vid = v.id, .eid = emin});
                std::push_heap(heap.begin(), heap.end(), heapComp);
            }

            for (int i = 1; i < vertices.size(); ++i) {

                std::pop_heap(heap.begin(), heap.end(), heapComp);
                const int key = heap.back().key;
                const int vid = heap.back().vid;
                const int eid = heap.back().eid;
                heap.pop_back();

                x[edges[eid].to] = true;
                result.push_back(std::make_pair(edges[eid].from, edges[eid].to));

                for (int e : vertices[vid].edges) {
                    if (edges[e].from == vid) {
                        auto it = std::find_if(heap.begin(), heap.end(), [this, e](const HeapItem& item) {
                            return item.vid == edges[e].to;
                        });
                        if (it != heap.end()) {
                            const int newKey = std::min(it->key, edges[e].weight);
                            heap.erase(it);
                            heap.push_back(HeapItem{.key = newKey, .vid = edges[e].to, .eid = e});
                            std::make_heap(heap.begin(), heap.end(), heapComp);
                        }
                    }
                }
            }
            return result;
        };

        return heapImpl();
    }

    bool hasCycles() {

        if (Directed) {
            throw IllegalStateException("Works only for undirected graphs");
        }

        std::vector<int> explored(vertices.size());
        for (const Vertex& v : vertices) {
            explored[v.id] = false;
        }

        std::stack<std::pair<int, int>> q;
        const int startVertex = 0;
        q.push(std::make_pair(startVertex, -1));
        explored[startVertex] = true;

        while (!q.empty()) {
            const int vid = q.top().first;
            const int parent = q.top().second;
            q.pop();
            const Vertex& vertex = vertices.at(vid);
            for (int edgeId : vertex.edges) {
                const Edge& edge = edges.at(edgeId);
                int adjucent = edge.from != vid ? edge.from : edge.to;
                if (!explored[adjucent]) {
                    explored[adjucent] = true;
                    q.push(std::make_pair(adjucent, vid));
                } else {
                    if (adjucent != parent) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    std::vector<std::pair<int, int>> kraskalMST() {

        if (Directed) {
            throw IllegalStateException("Works only for undirected graphs");
        }

        auto genericImpl = [this]() {
            std::vector<std::pair<int, int>> result;
            std::vector<Edge> sortedEdges(edges);
            std::sort(sortedEdges.begin(), sortedEdges.end(), [](const Edge& x, const Edge& y){
                return x.weight < y.weight;
            });

            Graph<false> t(vertices.size());

            for (const Edge& e : sortedEdges) {
                Graph<false> t2(t);
                t2.addEdge(e.from, e.to, e.weight);
                if (!t2.hasCycles()) {
                    result.push_back(std::make_pair(e.from, e.to));
                    t.addEdge(e.from, e.to, e.weight);
                }
            }

            return result;
        };

        return genericImpl();
    }

    std::vector<int> bellmanFord(int source) {
        const int n = vertices.size();
        int a[n][n];

        const int max_int = std::numeric_limits<int>::max();

        for (int i = 0; i < n; ++i) {
            a[0][i] = max_int;
        }
        a[0][source] = 0;

        int lasti = n - 1 ;
        for (int i = 1; i < n; ++i) {
            bool smthChanged = false;
            for (int v = 0; v < n; ++v) {
                const int case1 = a[i-1][v];
                int case2 = max_int;
                for (int eid : vertices[v].edges) {
                    const Edge& e = edges[eid];
                    if (Directed && e.from != v) {
                        continue;
                    }
                    const int w = e.from != v ? e.from : e.to;
                    const int case2Pretender = a[i-1][w] != max_int ? a[i-1][w] + e.weight : max_int;
                    if (case2Pretender < case2) {
                        case2 = case2Pretender;
                    }
                }
                a[i][v] = std::min(case1, case2);
                smthChanged |= a[i][v] == case2 && a[i][v] != case1;
            }
            if (!smthChanged) {
                lasti = i;
                break;
            }
        }

        std::vector<int> result(n);
        for (int i = 0; i < n; ++i) {
            result[i] = a[lasti][i];
        }
        return result;
    }

    std::vector<std::vector<int>> floydWarshall() {
        int n = vertices.size();
        int a[n+1][n][n];

        const int int_max = std::numeric_limits<int>::max();

        for (int v = 0; v < n; ++v) {
            for (int w = 0; w < n; ++w) {

                int minEdgeId = -1;
                for (int eid : vertices[v].edges) {
                    if (edges[eid].from == v && edges[eid].to == w) {
                        if (minEdgeId == -1 || edges[eid].weight < edges[minEdgeId].weight) {
                            minEdgeId = eid;
                        }
                    }
                }

                a[0][v][w] = v == w ? 0 :
                             minEdgeId != -1 ? edges[minEdgeId].weight :
                             int_max;
            }
        }

        for (int i = 1; i <= n; ++i) {
            for (int v = 0; v < n; ++v) {
                for (int w = 0; w < n; ++w) {
                    const int k = i - 1;
                    const int case1 = a[i-1][v][w];
                    const int case2 = (a[i-1][v][k] != int_max && a[i-1][k][w] != int_max)
                            ? (a[i-1][v][k] + a[i-1][k][w])
                            : int_max;
                    a[i][v][w] = std::min(case1, case2);
                }
            }
        }

        std::vector<std::vector<int>> result;

        for (int v = 0; v < n; ++v) {
            result.push_back(std::vector<int>());
            std::vector<int>& rr = result.back();
            for (int w = 0; w < n; ++w) {
                rr.push_back(a[n][v][w]);
            }
        }
        return result;
    }

    std::vector<int> astar(int source, int target) {

        std::vector<int> distance(vertices.size(), -1);
        distance[source] = 0;
        std::vector<int> parent(vertices.size(), -1);
        std::vector<bool> processed(vertices.size(), false);
        //std::vector<int> h(vertices.size(), 0);
        std::vector<int> h {0, 45, 40, 20, 20, 40, 0};

        struct HeapItem {
            int v; int f;
        };

        std::vector<HeapItem> heap;
        auto heapc = [](const HeapItem& x, const HeapItem& y) {
            return x.f > y.f;
        };

        heap.push_back(HeapItem{.v = source, .f = h[source]});
        std::push_heap(heap.begin(), heap.end(), heapc);

        while (!heap.empty()) {

            std::pop_heap(heap.begin(), heap.end(), heapc);
            const int v = heap.back().v;
            heap.pop_back();
            std::pop_heap(heap.begin(), heap.end(), heapc);
            processed[v] = true;

            for (int eid : vertices[v].edges) {
                if (edges[eid].from == v) {
                    if (!processed[edges[eid].to]) {
                        if (distance[edges[eid].to] == -1 || distance[edges[eid].to] > distance[v] + edges[eid].weight) {
                            distance[edges[eid].to] = distance[v] + edges[eid].weight;
                            parent[edges[eid].to] = v;
                            const int f = distance[edges[eid].to] + h[edges[eid].to];

                            auto it = std::find_if(heap.begin(), heap.end(), [this, eid](const HeapItem& heapItem) {
                                return heapItem.v == edges[eid].to;
                            });
                            if (it != heap.end()) {
                                heap.erase(it);
                            }
                            heap.push_back(HeapItem {.v = edges[eid].to, .f = f});
                            std::make_heap(heap.begin(), heap.end(), heapc);
                        }
                    }
                }
            }
            if (distance[target] != -1) {
                break;
            }
        }

        std::vector<int> path;
        if (distance[target] == -1) {
            return path;
        }

        for (int v = target; v != source;) {
            path.push_back(v);
            v = parent[v];
        }

        std::reverse(path.begin(), path.end());

        return path;
    }


private:
    std::vector<Vertex> vertices;
    std::vector<Edge> edges;
};


int knapsack(std::vector<int> values, std::vector<int> weights, int capacity) {

    if (values.size() != weights.size()) {
        throw IllegalStateException("Inconsistent sizes of input arrays");
    }

    const int n = values.size();

    int a[n+1][capacity + 1];

    for (int i = 0; i <= capacity; ++i) {
        a[0][i] = 0;
    }

    for (int i = 1; i <= n; ++i) {
        for (int j = 0; j <= capacity; ++j) {
            const int pj = j-weights[i-1];
            const int case1 = a[i-1][j];
            const int case2 = pj >= 0 ? a[i-1][pj] + values[i-1] : 0;
            a[i][j] = std::max(case1, case2);
        }
    }

    return a[n][capacity];
}

std::vector<int> getLongestCommonSubsequence(const std::vector<int>& s1, const std::vector<int>& s2) {

    const int n = s1.size();
    const int m = s2.size();
    int a[n + 1][m + 1];
    for (int i = 0; i <= m; ++i) {
        a[0][i] = 0;
    }
    for (int i = 0; i <= n; ++i) {
        a[i][0] = 0;
    }

    int niter = n + m - 1;
    for (int i = 0; i < niter; ++i) {
        int u = std::max(0, i - n + 1);
        for (int j = u; j < niter; ++j) {
            int x = i - j + 1;
            int y = j + 1;
            if (x <= 0 || y >= m + 1) {
                break;
            }
            if (s1[x-1] == s2[y-1]) {
                a[x][y] = a[x-1][y-1] + 1;
            } else {
                int case0 = a[x-1][y];
                int case1 = a[x][y-1];
                a[x][y] = std::max(case0, case1);
            }
        }
    }

    std::vector<int> iresult;
    iresult.reserve(std::min(n, m));
    int x = n;
    int y = m;
    while (x > 0 && m > 0) {
        if (a[x][y] == a[x-1][y]) {
            --x;
        } else if (a[x][y] == a[x][y-1]) {
            --y;
        } else {
            iresult.push_back(s1[x-1]);
            --x; --y;
        }
    }

    std::vector<int> result;
    result.reserve(iresult.size());

    for (auto it = iresult.rbegin(); it != iresult.rend(); ++it) {
        result.push_back(*it);
    }

//    for (int i = 0; i <= n; ++i) {
//        for (int j = 0; j <= m; ++j) {
//            std::cout << a[i][j] << " ";
//        }
//        std::cout << std::endl;
//    }
    return result;
}

void make_diag(int n, int m) {
    int iter_count = n + m - 1;

    int a[n][m];

    int v = 0;
    for (int i = 0; i < iter_count; ++i) {
        int u = std::max(0, i - n + 1);
        for (int j = u; j < iter_count; ++j) {
            int x = i - j;
            int y = j;
            if (x < 0 || y >= m) {
                break;
            }
            a[x][y] = v++;
        }
    }

    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            printf("%2.0d ", a[j][i]);
        }
        printf("\n");
    }
    printf("\n");
}


} // end of namespace tim

void run_tim() {
    using namespace tim;

    srand (time(NULL));

    std::cout << "run_tim >>>> " << std::endl;

    make_diag(8, 5);

    std::vector<int> data { 1, 4, 5, 2, 9, 3, 3, 4, 2, 8  };

    std::for_each(data.begin(), data.end(), [](int i){ std::cout << i << " "; });
    std::cout << std::endl;

    std::cout << "7th statistic is " << get_nth_statictic(data, 7) << std::endl;

    std::cout << "quick sort: ";
    std::random_shuffle(data.begin(), data.end());
    quick_sort(data);
    std::for_each(data.begin(), data.end(), [](int i) {std::cout << i << " "; });
    std::cout << std::endl;

    std::cout << "merge sort: ";
    std::random_shuffle(data.begin(), data.end());
    merge_sort(data);
    std::for_each(data.begin(), data.end(), [](int i) { std::cout << i << " "; });
    std::cout << std::endl;

    std::cout << "insertion sort: ";
    std::random_shuffle(data.begin(), data.end());
    insertion_sort(data);
    std::for_each(data.begin(), data.end(), [](int i) { std::cout << i << " "; });
    std::cout << std::endl;

    Graph<false> g(8);
    g.addEdge(0, 1); g.addEdge(0, 2); g.addEdge(0, 3);
    g.addEdge(1, 2); g.addEdge(1, 3);
    g.addEdge(2, 3); g.addEdge(2, 4);
    g.addEdge(3, 5);
    g.addEdge(4, 5); g.addEdge(4, 6); g.addEdge(4, 7);
    g.addEdge(5, 6); g.addEdge(5, 7);
    g.addEdge(6, 7);
    g.print();
    std::cout << "Minimum cut: " << g.find_minimum_cut() << std::endl;

    g.bfs_print(0);
    g.dfs_print(0);

    Graph<true> g2(4);
    g2.addEdge(3, 2);
    g2.addEdge(1, 0);
    g2.addEdge(3, 1);
    g2.addEdge(2, 0);

    auto order = g2.topological_sort();
    std::cout << "topological sort: ";
    std::copy(order.begin(), order.end(), std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;

    Graph<true> g4(11);
    g4.addEdge(0, 1);
    g4.addEdge(1, 2); g4.addEdge(1, 3);
    g4.addEdge(2, 0); g4.addEdge(2, 4); g4.addEdge(2, 5);
    g4.addEdge(3, 8); g4.addEdge(3, 9);
    g4.addEdge(4, 6); g4.addEdge(4, 7); g4.addEdge(4, 8);
    g4.addEdge(5, 4);
    g4.addEdge(6, 7); g4.addEdge(6, 10);
    g4.addEdge(7, 5);
    g4.addEdge(8, 9);
    g4.addEdge(9, 10);
    g4.addEdge(10, 8);
    auto scc = g4.find_strongly_connected_components();

    std::cout << "scc: ";
    std::copy(scc.begin(), scc.end(), std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;

    Graph<true> g5(4);
    g5.addEdge(0, 1);
    g5.addEdge(0, 2);
    g5.addEdge(1, 2); g5.addEdge(1, 3);
    g5.addEdge(2, 3);
    auto bfsSP = g5.bfsShortestPath(0);
    std::cout << "bfsShortestPaths: ";
    std::copy(bfsSP.begin(), bfsSP.end(), std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;

    Graph<true> g6(4);
    g6.addEdge(0, 1, 1);
    g6.addEdge(0, 2, 4);
    g6.addEdge(1, 2, 2);
    g6.addEdge(1, 3, 6);
    g6.addEdge(2, 3, 3);
    auto dijkstraSP = g6.dijkstra(0);
    std::cout << "dijkstra: ";
    std::copy(dijkstraSP.begin(), dijkstraSP.end(), std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;

    Graph<false> g7(5);
    g7.addEdge(0, 1, 1); g7.addEdge(0, 2, 4); g7.addEdge(0, 3, 3);
    g7.addEdge(1, 3, 2);
    g7.addEdge(2, 3, 5);
    g7.addEdge(2, 4, 6);
    auto primMST = g7.primMST();
    std::cout << "primMST: ";
    std::for_each(primMST.begin(), primMST.end(), [](std::pair<int, int> e) {
        std::cout << "(" << e.first << ", " << e.second << ") ";
    });
    std::cout << std::endl;

    Graph<false> g8(5);
    g8.addEdge(0, 1, 1); g8.addEdge(0, 2, 5); g8.addEdge(0, 3, 4); g8.addEdge(0, 4, 3);
    g8.addEdge(1, 2, 7);
    g8.addEdge(2, 4, 6);
    g8.addEdge(3, 4, 2);
    auto kraskalMST = g8.kraskalMST();
    std::cout << "kraskalMST: ";
    std::for_each(kraskalMST.begin(), kraskalMST.end(), [](std::pair<int, int> e) {
        std::cout << "(" << e.first << ", " << e.second << ") ";
    });
    std::cout << std::endl;

    auto ks = knapsack(std::vector<int>{3, 2, 4, 4}, std::vector<int>{4, 3, 2, 3}, 6);
    std::cout << "knapsack: " << ks << std::endl;

    std::vector<int> commonSubsequence = getLongestCommonSubsequence(
                std::vector<int>{1, 2, 3, 4, 1},
                std::vector<int>{3, 4, 1, 2, 1, 3});
    std::cout << "common subsequence: ";
    std::for_each(commonSubsequence.begin(), commonSubsequence.end(), [](const int& i) {
        std::cout << i << " ";
    });
    std::cout << std::endl;

    Graph<false> g9(5);
    g9.addEdge(0, 1, 2); g9.addEdge(0, 2, 4);
    g9.addEdge(1, 2, 1); g9.addEdge(1, 3, 2);
    g9.addEdge(2, 4, 4);
    g9.addEdge(3, 4, 2);

    auto bf = g9.bellmanFord(0);
    std::cout << "Bellman-Ford: ";
    std::copy(bf.begin(), bf.end(), std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;

    auto fw = g9.floydWarshall();
    std::cout << "Floyd-Warshall: " << std::endl;
    for (int i = 0; i < fw.size(); ++i) {
        std::cout << "        " << i << ": ";
        for (int p : fw[i]) {
            std::cout << p << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    Graph<true> g10(7);
    g10.addEdge(0, 1, 20); g10.addEdge(0, 2, 15);
    g10.addEdge(1, 3, 30);
    g10.addEdge(2, 4, 20);
    g10.addEdge(3, 6, 20);
    g10.addEdge(4, 5, 30);
    g10.addEdge(5, 6, 40);
    auto astar = g10.astar(0, 6);
    std::cout << "astar: ";
    std::copy(astar.begin(), astar.end(), std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;

    std::cout << "run_tim <<<< " << std::endl;
}
