#include <iostream>

namespace problem_set_1 {

using namespace std;

void fizzBuzz() {
    int number_of_lines = 0;
    cin >> number_of_lines;

    for (int i = 0; i < number_of_lines; ++i) {
        const int value = i + 1;
        const bool multiples3 = (value % 3) == 0;
        const bool multiples5 = (value % 5) == 0;
        if (multiples3) {
            cout << "Fizz";
        }
        if (multiples5) {
            cout << "Buzz";
        }
        if (!multiples3 && !multiples5) {
            cout << value;
        }
        cout << endl;
    }
}

} // end of namespace problem_set_1

void run_problem_set_1() {
    using namespace problem_set_1;
    using namespace std;

    cout << "run_problem_set_1 >>>> " << endl;
    fizzBuzz();
    cout << "run_problem_set_1 <<<< " << endl;
}
