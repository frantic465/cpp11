#include <iostream>
#include <functional>

namespace test_functional {

using namespace std;
using namespace std::placeholders;

double some_function(int a, int b) {
    cout << "some_function(" << a << ", " << b << ")" << endl;
    ++a;
    ++b;
    return 0.7;
}

void test_1() {
    shared_ptr<int> intRef = make_shared<int>(5);

    cout << *intRef << endl;

    int k = 0, m = 0;

    function<double(int)> f_test = [&k, m](int val) mutable -> double {
        cout << "Functor test: val=" << val << ", k=" << k << endl;
        k = 1;
        m = 1;
        return 0.5;
    };

    cout << f_test(1) << endl;
    cout << "k=" << k << ", m=" << m << endl;

    f_test = bind(&some_function, _1, k);
    cout << f_test(k) << endl;
    cout << "k=" << k << ", m=" << m << endl;
}

} // end of namespace test_functional


void run_test_functional() {
    using namespace test_functional;
    using namespace std;
    cout << "run_test_functional >>>> " << endl;
    test_1();
    cout << "run_test_functional <<<< " << endl;
}
