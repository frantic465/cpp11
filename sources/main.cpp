#include <iostream>
#include <chrono>
#include <memory>
#include <functional>
#include <vector>
#include <math.h>
#include <fstream>
#include <list>
#include <set>
#include <thread>
#include "entries.h"

class A {
public:
    A() {
        printf("A::ctor\n"); fflush(stdout);
    }
    ~A() {
        printf("A::dtor\n"); fflush(stdout);
    }

    A(const A&& a) {
        printf("A::mctor\n"); fflush(stdout);
    }

    A(const A& a) {
        printf("A::cctor\n"); fflush(stdout);
    }

    A& operator=(const A&&) {
        printf("A::massignment\n"); fflush(stdout);
    }

    A& operator=(const A&) {
        printf("A::cassignment\n"); fflush(stdout);
    }
};

void check_endianness() {
    union {
        char c[4];
        int a;
    } s;
    s.a = 1;
    printf("%s-endian\n", s.c[0] ? "little" : "big");
}

int main() {

    check_endianness();
    //run_test_functional();
    //run_test_thread();
    //run_tim();
    //run_test_io();

    //run_sutter();
    //run_test_memory();

    //run_linux_io();

    //run_test_atomic();

    run_test_collections();

    return 0;
}
