#include <iostream>
#include <sstream>
#include <initializer_list>
#include <vector>

bool isOdd(int a) {
    return a % 2 != 0;
}

void run_test_io() {

    int i;
    int s;

    std::cout << std::showpos << 15 << " " << 7 << std::noshowpos << std::endl;
    std::cout << std::hex << std::showbase << 15 << " " <<  7 << std::dec << std::noshowbase << std::endl;

    std::cout.setf(std::ios::showbase | std::ios::hex, std::ios::showbase | std::ios::basefield);
    std::cout << 15 << " " <<  7 << std::endl;
    std::cout.unsetf(std::ios::showbase | std::ios::basefield);

    std::cout << 15 << " " <<  7 << std::endl;

    std::vector<int> f;
    f.back();

    std::stringstream ss;
    ss << "aaaa == " << 7 << std::endl;

    std::cout << ss.rdbuf();

    std::string line;
    while (std::getline(std::cin, line)) {
        std::cout << "(" << line << ")" << std::endl;
    }

    bool good = std::cin.good();
    bool fail = std::cin.fail();
    bool eof = std::cin.eof();
    bool bad = std::cin.bad();
    std::cout << "good: " << good << std::endl;
    std::cout << "fail: " << fail << std::endl;
    std::cout << "eof: " << eof << std::endl;
    std::cout << "bad: " << bad << std::endl;


}
