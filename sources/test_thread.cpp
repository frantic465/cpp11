#include <chrono>
#include <iostream>
#include <thread>
#include <numeric>
#include <future>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <list>
#include <stdio.h>

namespace test_thread {

using namespace std;

void test_timers() {
    cout << "test_timers >>>>" << endl;
    auto now = chrono::high_resolution_clock::now().time_since_epoch().count();

    cout << "now: " << now << endl;

    auto t1 = chrono::high_resolution_clock::now();
    this_thread::sleep_for(chrono::milliseconds(500));
    auto t2 = chrono::high_resolution_clock::now();
    cout << "delay: " << chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << endl;
    cout << "test_timers <<<<" <<  endl;
}

void test_threads0() {
    std::vector<int> data(50, 1);

    std::packaged_task<int()> task0([&data]() {
        return std::accumulate(data.begin(), data.begin() + 25, 0);
    });

    std::packaged_task<int()> task1([&data]() {
        return std::accumulate(data.begin() + 25, data.end(), 0);
    });

    auto sum0 = task0.get_future();
    auto sum1 = task1.get_future();

    thread t0 (std::move(task0));
    thread t1 (std::move(task1));

    std::cout << "sum: " << sum0.get() + sum1.get() << std::endl;

    t0.detach();
    t1.detach();
}

void test_threads1() {
    std::vector<int> data(50, 1);

    std::promise<int> sum0_p;
    thread t0([&sum0_p, &data](){
       sum0_p.set_value(std::accumulate(data.begin(), data.begin() + 25, 0));
    });

    t0.detach();

    std::promise<int> sum1_p;
    thread t1([&sum1_p, &data](){
        sum1_p.set_value(std::accumulate(data.begin() + 25, data.end(), 0));
    });

    t1.detach();

    std::cout << "sum: " << sum0_p.get_future().get() + sum1_p.get_future().get() << std::endl;
}

void test_cond_variables() {

    std::condition_variable cond;
    std::mutex m;
    bool running = false;

    thread t([&]() {
        auto t0 = std::chrono::high_resolution_clock::now();
        std::unique_lock<std::mutex> lock(m);
        cond.wait(lock, [&running](){ return running; });
        auto t1 = std::chrono::high_resolution_clock::now();
        std::cout << "test_cond_variables: waited for "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
                  << std::endl;
    });

    thread go([&]() {
        this_thread::sleep_for(chrono::seconds(1));
        std::unique_lock<std::mutex> lock(m);
        running = true;
        cond.notify_one();
    });

    go.join();
    t.join();

}

void test_threads2() {

    class BgRoutine {
    public:
        void operator()() const {
            printf("BgRoutine\n");
        }
    };

    std::thread t{BgRoutine()};

    std::thread t2([]() {
        printf("do smth else\n");
    });

    t.join();
    t2.join();
}

void test_locks() {
    std::mutex m1;
    std::mutex m2;

    std::thread t1([&m1, &m2](){
        std::lock(m1, m2);
        std::lock_guard<std::mutex> l1(m1, std::adopt_lock);
        printf("test_locks t1 - m1\n"); std::cout.flush();
        this_thread::sleep_for(chrono::milliseconds(100));
        std::lock_guard<std::mutex> l2(m2, std::adopt_lock);
        printf("test_locks t1 - m2\n"); std::cout.flush();
    });

    std::thread t2([&m1, &m2](){
        std::lock(m2, m1);
        std::lock_guard<std::mutex> l1(m2, std::adopt_lock);
        printf("test_locks t2 - m2\n"); std::cout.flush();
        this_thread::sleep_for(chrono::milliseconds(100));
        std::lock_guard<std::mutex> l2(m1, std::adopt_lock);
        printf("test_locks t2 - m1\n"); std::cout.flush();
    });

    t1.join();
    t2.join();
}

void test_call_once() {

    std::once_flag flag;

    class Initializer {
    public:
        void run() {
           printf("resource initialized\n");
        }
    } initializer;

    auto async_routine = [&flag, &initializer]() {

        std::call_once(flag, &Initializer::run, initializer);
        printf("thread routine\n");
        fflush(stdout);
    };

    std::thread t0 (async_routine);
    std::thread t1 (async_routine);

    t0.join();
    t1.join();
}

void test_future() {
    printf("test future on %d\n", std::this_thread::get_id());

    std::shared_future<int> f0 = std::async(std::launch::deferred, []() {
       std::this_thread::sleep_for(std::chrono::milliseconds(500));
       printf("f0 on %d\n", std::this_thread::get_id());
       return 5;
    });

    std::packaged_task<int()> p1 = std::packaged_task<int()>([](){
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        printf("f1 on %d\n", std::this_thread::get_id());
        return 6;
    });
    std::future<int> f1 = p1.get_future();
    std::thread t1(std::move(p1));

    std::promise<int> p2;
    std::future<int> f2 = p2.get_future();
    std::thread t2([&p2](){
        std::this_thread::sleep_for(std::chrono::milliseconds(1500));
        printf("f2 on %d\n", std::this_thread::get_id());
        p2.set_value(7);
    });

    printf("future2 is ready: %d\n", f2.wait_for(std::chrono::milliseconds(100))
                                        == std::future_status::ready);

    printf("future0: %d\n", f0.get());
    printf("future1: %d\n", f1.get());
    printf("future2: %d\n", f2.get());

    printf("future0 is valid: %d\n", f0.valid());
    printf("future1 is valid: %d\n", f1.valid());

    t1.join(); t2.join();
}

template<typename F, typename... Args>
std::future<typename std::result_of<F(Args...)>::type>
spawn_task(F&& f, Args&&... args) {
    typedef typename std::result_of<F(Args...)>::type result_type;
    std::packaged_task<result_type(Args...)> task(std::move(f));
    std::future<result_type> res = task.get_future();
    std::thread t(std::move(task), args...);
    t.detach();
    return res;
}

template <typename T>
std::list<T> quick_sort_parallel(std::list<T> a) {
    if (a.size() <= 1) {
        return a;
    }
    std::list<T> result;
    result.splice(result.end(), a, a.begin());
    T pivot = *result.begin();
    auto part_point = std::partition(a.begin(), a.end(), [&pivot](const T& item){ return item < pivot; });
    std::list<T> left;
    left.splice(left.end(), a, a.begin(), part_point);

    auto new_left = spawn_task(&quick_sort_parallel<T>, std::move(left));
    auto new_right = quick_sort_parallel(a);

    result.splice(result.begin(), new_left.get());
    result.splice(result.end(), new_right);
    return result;
}

void test_paraller_sort() {
    std::list<int> datalist { 1, 4, 5, 2, 9, 3, 3, 4, 2, 8  };
    auto sorted_data = quick_sort_parallel(datalist);
    std::for_each(sorted_data.begin(), sorted_data.end(), [](int i) {std::cout << i << " "; });
    std::cout << std::endl;
}

} // end of namespace test_thread

void run_test_thread() {

    using namespace test_thread;
    using namespace std;

    std::cout << sizeof(float) << std::endl;

    cout << "test_thread >>>>" << endl;
    //test_timers();
    //test_threads0();
    //test_threads1();
    //test_threads2();
    //test_locks();
    //test_call_once();
    //test_cond_variables();
    //test_future();
    test_paraller_sort();

    cout << "test_thread <<<<" << endl;
}
