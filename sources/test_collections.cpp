#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <bitset>
#include <unordered_set>
#include <unordered_map>
#include <iterator>
#include <algorithm>
#include <functional>

template <typename T>
void cprint(T collection, std::string name) {
    std::cout << name << ": ";
    std::for_each(collection.begin(), collection.end(), [](const typename T::value_type& v){
        std::cout << v << " ";
    });
    std::cout << std::endl;
}

template <typename T>
void cprintm(T collection, std::string name) {
    std::cout << name << ": ";
    std::for_each(collection.begin(), collection.end(), [](const typename T::value_type& v){
        std::cout << "(" << v.first << ": " << v.second << ") ";
    });
    std::cout << std::endl;
}

void test_vector() {
    std::vector<int> v1{1, 2, 3, 4};
    std::vector<int> v2{5, 6, 7, 8};
    std::vector<int> v3(v1);
    std::vector<int> v4(v2.begin(), v2.end());
    std::vector<int> v5;
    v5.assign(v1.begin(), v1.end());

    v1.resize(5);
    v1.reserve(200);

    cprint(v1, "v1");
    cprint(v2, "v2");
    cprint(v3, "v3");
    cprint(v4, "v4");
    cprint(v5, "v5");

    std::cout << "size: " << v1.size() << std::endl;
    std::cout << "max_size: " << v1.max_size() << std::endl;
    std::cout << "capacity: " << v1.capacity() << std::endl;
    std::cout << "empty: " << v1.empty() << std::endl;
    v1.shrink_to_fit();
    std::cout << "capacity after shrinking: " << v1.capacity() << std::endl;

    v1.erase(v1.begin() + 3, v1.begin() + 4);
    cprint(v1, "v1(erase)");

    v1.emplace_back(666);
    cprint(v1, "v1(emplace_back)");

    std::vector<int>::iterator it = v1.begin();
    std::vector<int>::iterator rit = --v1.end();

    while (it <= rit) {
        std::cout << *it << " " << *rit << " ";
        it++;
        rit--;
    }
    std::cout << std::endl;
}

void test_list() {
    std::list<int> l1 {1, 2, 3, 4, 5, 6, 7};
    std::list<int> l2;
    l2.assign(5, 10);

    std::list<int> l3 {9, 1, 1};
    std::list<int>::iterator splice_end = l2.begin();
    std::advance(splice_end, 3);
    l3.splice(l3.end(), l2, l2.begin(), splice_end);

    cprint(l1, "l1");
    cprint(l2, "l2");
    cprint(l3, "l3");

    l3.remove(1);
    cprint(l3, "l3(remove 1)");
    l3.remove_if([](const int& el){ return el == 9; });
    cprint(l3, "l3(remove if equals 9)");
    l3.unique();
    cprint(l3, "l3(unique)");

    l1.reverse();
    cprint(l1, "l1(reverse)");

    std::list<int>::iterator it = l1.begin();
    std::list<int>::iterator rit = --l1.end();

    while (true) {
        std::cout << *it << " " << *rit << " ";
        it++;
        if (it == rit) break;
        rit--;
        if (it == rit) {
            std::cout << *it << " ";
            break;
        }
    }
    std::cout << std::endl;

    std::list<int>::iterator it2 = l1.begin();
    std::list<int>::reverse_iterator rit2 = l1.rbegin();

    std::cout << "rbegin " << *rit2 << std::endl;
    std::cout << "rbegin::base " << *(--rit2.base()) << std::endl;
    std::cout << "end == rbegin::base " << (bool)(l1.end() == l1.rbegin().base()) << std::endl;

    while (true) {
        std::cout << *it2 << " " << *rit2 << " ";
        it2++;
        if (it2 == --(rit2.base())) break;
        rit2++;
        if (it2 == --(rit2.base())) {
            std::cout << *it2 << " ";
            break;
        }
    }
    std::cout << std::endl;
}

void test_string() {
    std::string s1 = "test";
    s1.assign("Hello World");
    std::cout << s1 << std::endl;

    s1.append(3, '!');
    std::cout << s1 << std::endl;

    s1.replace(s1.find(' '), std::string::npos, " trololo");
    std::cout << s1 << std::endl;

    char buffer[5];
    s1.copy(buffer, sizeof(buffer), 0);
    std::cout << buffer << std::endl;
}

void test_set() {

    auto compare = [](const int& a, const int& b){
        return a < b;
    };

    std::set<int, decltype(compare)> s1 {{ 1, 2, 3, 4}, compare };

    cprint(s1, "s1");

    s1.insert({8, 0, 9, 1, 1});
    cprint(s1, "s1");

    std::cout << "set key_comp " << (s1.key_comp() == compare) << std::endl;
    std::cout << "set value_comp " << (s1.value_comp() == compare) << std::endl;

    auto it = s1.find(4);
    std::cout << "s1(find 4): ";
    std::copy(it, s1.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    auto it2 = s1.lower_bound(5);
    std::cout << "s1(lower_bound 5): " << *it2 << std::endl;

    auto it3 = s1.upper_bound(5);
    std::cout << "s1(upper_bound 5): " << *it3 << std::endl;

    auto range = s1.equal_range(2);
    std::cout << "s1(equal_range 2): ";
    std::copy(range.first, range.second, std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    auto range2 = s1.equal_range(5);
    std::cout << "s1(equal_range 5): ";
    std::copy(range2.first, range2.second, std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

void test_map() {

    auto compare = [](const int& a, const int& b) {
        return a < b;
    };

    std::map<int, std::string> m { {1, "one"}, {2, "two"}, {3, "three"}};

    cprintm(m, "m");
}

void test_stack_queue() {
    std::deque<int> d{ 9, 8, 7, 6, 5, 4, 3, 2, 1};
    std::stack<int, decltype(d)> s(d);
    s.emplace(666);

    std::cout << "s: ";
    while (!s.empty()) {
        std::cout << s.top() << " ";
        s.pop();
    }
    std::cout << std::endl;

    std::queue<int, decltype(d)> q(d);
    q.emplace(666);
    std::cout << "q: ";
    while (!q.empty()) {
        std::cout << q.front() << " ";
        q.pop();
    }
    std::cout << std::endl;

    std::vector<int> d2 {5, 1, 5, 2, 0, 4, 10, 6};
    std::priority_queue<int> pq(std::less<int>(), d2);
    pq.emplace(666);

    std::cout << "pq: ";
    while (!pq.empty()) {
        std::cout << pq.top() << " ";
        pq.pop();
    }
    std::cout << std::endl;
}

void test_unordered_map() {
    typedef std::unordered_map<int, std::string, std::hash<int>, std::equal_to<int>> Map;
    Map map { {1, "one"}, {3, "three"}, {5, "five"} };
    cprintm(map, "hash_map");

    std::cout << "hash_map(bucket_count): " << map.bucket_count() << std::endl;
    for (int i = 0; i < map.bucket_count(); ++i) {
        std::cout << "hash_map(bucket_size "<< i << "): " << map.bucket_size(i) << std::endl;
    }
    std::cout << std::endl;

    std::cout << "hash_map(bucket of 3): " << map.bucket(3) << std::endl;
    std::cout << "hash_map(bucket of 2): " << map.bucket(2) << std::endl;
}

void test_bitset() {
    std::bitset<10> b1;
    std::bitset<10> b2(0x7f);
    std::bitset<10> b3(073);
    std::bitset<10> b4("101110");

    std::cout << "b1: " << b1 << std::endl;
    std::cout << "b2: " << b2 << std::endl;
    std::cout << "b3: " << b3 << std::endl;
    std::cout << "b4: " << b4 << std::endl;

    std::cout << "b4(count): " << b4.count() << std::endl;
    std::cout << "b4(size): " << b4.size() << std::endl;
    std::cout << "b4(test 2): " << b4.test(2) << std::endl;
    std::cout << "b4(test 4): " << b4.test(4) << std::endl;
    std::cout << "b1(none): " << b1.none() << std::endl;
    std::cout << "b1(any): " << b1.any() << std::endl;
    std::cout << "b4(none): " << b4.none() << std::endl;
    std::cout << "b4(any): " << b4.any() << std::endl;

    b4.flip();
    std::cout << "b4(flip): " << b4 << std::endl;

}

void test_heap() {
    std::vector<int> v { 1, 5, 0, 4, 2, 8, 3};
    std::make_heap(v.begin(), v.end());
    v.push_back(6);
    std::push_heap(v.begin(), v.end());

    std::sort_heap(v.begin(), v.end());
    cprint(v, "v(heapified)");

    std::make_heap(v.begin(), v.end());
    std::cout << "heap: ";
    while (!v.empty()) {
        std::pop_heap(v.begin(), v.end());
        std::cout << v.back() << " ";
        v.pop_back();
    }
    std::cout << std::endl;
}

void test_algorithms() {
    std::vector<int> v0;
    for (int i = 0; i < 20; ++i) {
        v0.push_back(i);
    }
    std::random_shuffle(v0.begin(), v0.end());
    std::list<int> l0(v0.begin(), v0.end());

    std::random_shuffle(v0.begin(), v0.end());


    cprint(v0, "v0");
    cprint(l0, "l0");

    std::cout << "all_of: " <<
                 std::all_of(v0.begin(), v0.end(), [](const int& el) { return el >= 0; }) <<
                 std::endl;

    std::cout << "any_of: " <<
                 std::any_of(v0.begin(), v0.end(), [](const int& el) { return el == 12; }) <<
                 std::endl;

    std::cout << "none_of: " <<
                 std::none_of(v0.begin(), v0.end(), [](const int& el) { return el == 12; }) <<
                 std::endl;

    using namespace std::placeholders;

    std::cout << "find: " << (std::find(v0.begin(), v0.end(), 5) != v0.end()) << std::endl;
    std::cout << "find_if: " << (std::find_if(v0.begin(), v0.end(), [](const int& e){ return e == 5;}) != v0.end()) << std::endl;
    std::cout << "find_if_not: " << (std::find_if_not(v0.begin(), v0.end(), [](const int& e){ return e == 5;}) != v0.end()) << std::endl;
    std::cout << "find_end: " << (std::find(v0.begin(), v0.end(), 5) != v0.end()) << std::endl;

    std::vector<int> v1{1, 2};
    std::cout << "find_first_of: " <<
                 (std::find_first_of(v0.begin(), v0.end(), v1.begin(), v1.end()) != v0.end()) <<
                 std::endl;

    std::cout << "adjacent_find: " <<
                 (std::adjacent_find(v0.begin(), v0.end()) != v0.end()) <<
                 std::endl;

    std::cout << "count_if: " <<
                 std::count_if(v0.begin(), v0.end(), [](const int& e) { return e > 5; }) <<
                 std::endl;

    std::vector<int> v2(v0);
    std::cout << "equal: " <<
                 std::equal(v0.begin(), v0.end(), v2.begin()) <<
                 std::endl;

    v2[4] = 66;

    auto mismatch_res = std::mismatch(v0.begin(), v0.end(), v2.begin());
    std::cout <<"mismatch: " <<
                *(mismatch_res.first) << " != " << *(mismatch_res.second) <<
                std::endl;

    std::cout << "is_permutation: " <<
                 std::is_permutation(v0.begin(), v0.end(), l0.begin()) <<
                 std::endl;

    std::vector<int> v3(v0.size());
    v3.erase(std::copy_if(v0.begin(), v0.end(), v3.begin(), [](const int& e) { return e % 2 == 0; }),
             v3.end());
    cprint(v3, "v3(copy_if)");

    std::vector<int> v4(v0.size());
    std::move(v0.begin(), v0.end(), v4.begin());
    cprint(v4, "v4(move)");

    std::transform(v0.begin(), v0.end(), v0.begin(), [](const int& e) {
        return e * 2;
    });
    cprint(v0, "transform");

    std::transform(v0.begin(), v0.end(), v4.begin(), v4.begin(), [](const int& a, const int& b){
       return a + b;
    });
    cprint(v4, "transform");

    std::replace(v0.begin(), v0.end(), 12, 777);
    cprint(v0, "replace");

    std::generate(v0.begin(), v0.end(), []() {
        return rand() % 10;
    });
    cprint(v0, "generate");

    v0.erase(std::remove_if(v0.begin(), v0.end(), [](const int& e) { return e % 2 == 0; }),
             v0.end());
    cprint(v0, "remove_if");

    v0.erase(std::unique(v0.begin(), v0.end()),
             v0.end());
    cprint(v0, "unique");
}

void run_test_collections() {
    std::cout << "run_test_collections >>>> " << std::endl;

    //test_vector();
    test_list();
    //test_string();
    //test_set();
    //test_map();
    //test_stack_queue();
    //test_unordered_map();
    //test_bitset();
    //test_heap();
    test_algorithms();

    std::cout << "run_test_collections <<<< " << std::endl;
}
