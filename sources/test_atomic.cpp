#include <iostream>
#include <thread>
#include <atomic>
#include <vector>
#include <string>
#include <sstream>

void test_atomic_flag() {
    std::stringstream s;
    std::atomic_flag s_flag = ATOMIC_FLAG_INIT;

    std::cout << "s_flag = " << s_flag.test_and_set() << std::endl;
    std::cout << "s_flag = " << s_flag.test_and_set() << std::endl;
    s_flag.clear();

    auto printval = [&](int i) {
        while (s_flag.test_and_set()) {}
        s << "thread " << i << "\n";
        s_flag.clear();
    };

    std::vector<std::thread> threads;
    for (int i = 0; i < 10; ++i) {
        threads.push_back(std::thread(printval, i));
    }

    for (auto& t : threads) {
        t.join();
    }

    std::cout << s.str() << std::endl;
}

void test_atomic_bool() {
    std::stringstream s;
    std::atomic<bool> s_flag(false);
    std::cout << "atomic<bool> is lock free " << s_flag.is_lock_free() << std::endl;

    auto printval = [&](int i) {
        while (s_flag.exchange(true)) {}
        s << "thread " << i << "\n";
        s_flag = false;
    };

    std::vector<std::thread> threads;
    for (int i = 0; i < 10; ++i) {
        threads.push_back(std::thread(printval, i));
    }

    for (auto& t : threads) {
        t.join();
    }

    std::cout << s.str() << std::endl;
}

void test_atomic_ptr() {

    struct Node {
        Node* next;
        int value;
    };

    std::atomic<Node*> head(nullptr);

    std::vector<std::thread> threads;

    auto additem = [&](int i) {

        Node* old_head = head;
        Node* new_node = new Node();
        new_node->next = old_head;
        new_node->value = i;

        while (!head.compare_exchange_weak(old_head, new_node)) {
            new_node->next = old_head;
        }
    };

    for (int i = 0; i < 10; ++i) {
        threads.push_back(std::thread(additem, i));
    }

    for (auto& t : threads) {
        t.join();
    }

    for (Node* it = head; it != nullptr; it = it->next) {
        std::cout << it->value << " ";
    }
    std::cout << std::endl;
}

void test_nonblk_counter() {
    class Counter {
    public:
        Counter() : c(0) {
        }

        unsigned get() {
            return c.load();
        }

        unsigned inc() {
            unsigned old_val = c.load();
            unsigned new_val = old_val + 1;

            while (!c.compare_exchange_weak(old_val, new_val)) {
                new_val = old_val + 1;
            }

            return old_val;
        }

    private:
        std::atomic<unsigned> c;
    } counter;

    std::vector<std::thread> threads;

    for (int i = 0; i < 50; ++i) {
        threads.push_back(std::thread(&Counter::inc, &counter));
    }

    for (auto& t : threads) {
        t.join();
    }

    std::cout << "counter: " << counter.get() << std::endl;
}

void run_test_atomic() {
    std::cout << "run_test_atomic >>>>" << std::endl;

    //test_atomic_flag();
    test_atomic_bool();
    test_atomic_ptr();
    test_nonblk_counter();

    std::cout << "run_test_atomic <<<<" << std::endl;
}
