#include <iostream>
#include <memory>

void run_test_memory() {
    std::cout << "run_test_memory >>>> " << std::endl;
    class A {
    private:
        const char* const name;
    public:
        A(const char* name) : name(name) {
            printf("A::ctor(%s)\n", name);
        }

        A() : name(0) {
            printf("A::ctor(%s)\n", name);
        }

        ~A() {
            printf("A::dtor(%s)\n", name);
        }
    };

    std::shared_ptr<A> a1 = std::make_shared<A>("obj0");
    std::unique_ptr<A> a2;
    std::unique_ptr<A> a3(new A("obj1"));
    a2 = std::move(a3);

    std::weak_ptr<A> a4(a1);

    std::shared_ptr<A> a5 = a4.lock();

    printf("obj0: refcounter=%d\n", a1.use_count());

    std::shared_ptr<A> a6 (new A[5], std::default_delete<A[]>());
    std::cout << "run_test_memory <<<< " << std::endl;
}
