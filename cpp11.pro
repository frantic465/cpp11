TEMPLATE = app
TARGET = cpp11

DIR_ROOT = $${system(pwd)}

QT -= core gui

SOURCES +=  sources/main.cpp \
    sources/test_functional.cpp \
    sources/test_thread.cpp \
    sources/problem_set_1.cpp \
    sources/tim.cpp \
    sources/test_io.cpp \
    sources/sutter.cpp \
    sources/linux_io.cpp \
    sources/test_memory.cpp \
    sources/test_atomic.cpp \
    sources/test_collections.cpp

HEADERS +=  sources/entries.h

OTHER_FILES += Makefile
